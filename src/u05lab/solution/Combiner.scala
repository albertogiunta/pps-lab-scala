package u05lab.solution

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}

trait Combiner[A] {

  def unit: A

  def combine(a: A, b: A): A
}

case class DoubleAdder() extends Combiner[Double]{

  override def unit: Double = 0

  override def combine(a: Double, b: Double): Double = a+b
}

case class StringConcatenator() extends Combiner[String]{

  override def unit: String = ""

  override def combine(a: String, b: String): String = a concat b
}

case class IntMaximizer() extends Combiner[Int]{

  override def unit: Int = Int.MinValue

  override def combine(a: Int, b: Int): Int = a max b
}

object FunctionsImpl extends Functions {

  private def combine[A](as: Traversable[A])(combiner: Combiner[A]): A = {
    var acc: A = combiner.unit
    as.foreach(a => acc = combiner.combine(acc,a))
    acc
  }

  override def sum(a: List[Double]): Double = combine(a)(new DoubleAdder)

  override def concat(a: Seq[String]): String = combine(a)(new StringConcatenator)

  override def max(a: List[Int]): Int = combine(a)(new IntMaximizer)
}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0,20.0,30.1))) // 60.1
  println(f.sum(List()))                // 0.0
  println(f.concat(Seq("a","b","c")))   // abc
  println(f.concat(Seq()))              // ""
  println(f.max(List(-10,3,-5,0)))      // 3
  println(f.max(List()))                // -2147483648
}